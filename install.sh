#!/bin/bash


cp ./.bashrc ..
cp ./.vimrc ..

if [ -d "../.vim" ]; then 
	rm -r ../.vim	
fi

cp -r ./.vim ..

