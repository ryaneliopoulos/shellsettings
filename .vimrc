set incsearch
set hlsearch
set number
set shiftwidth=2
set tabstop=4
set expandtab
set cursorline
set title
set background=dark
set backspace=indent,eol,start  " quote 'more powerful backspacing'


filetype plugin on
syntax on

"colorscheme dracula 
"
colorscheme PaperColor
"colorscheme gruvbox
set background=dark


" Python Scheme
autocmd Filetype python call SetPythonOptions()
function SetPythonOptions()
    colorscheme gruvbox
    set background=dark
endfunction

" C Scheme
autocmd Filetype c call SetCOptions()
function SetCOptions()
    colorscheme PaperColor
endfunction

autocmd Filetype make setlocal noexpandtab

"" Shell Script Scheme
autocmd Filetype sh call SetSHOptions()
function SetSHOptions ()
    colorscheme PaperColor
endfunction
